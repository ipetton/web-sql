drop database if exists training;
create database if not exists trainig;
use trainig;
drop table if exists people;

create table if not exists people (
	id int not null auto_increment primary key,
    surname varchar(20),
    amount int
);

insert into people (surname, amount) values 
						("Ivanov", 100),
                        ("Petrov", 200),
                        ("Sidorov", 150),
                        ("Ivanov", 800),
                        ("Petrov", 100),
                        ("Sidorov", 300);

select id, surname, min(amount) as min, max(amount) as max
from people
group by surname;

select surname, avg(amount) as amout
from people;

select surname, avg(amount) as amount
from people
group by surname;


(select surname, 'max' as Type, amount
from people
where amount in (select max(amount) from people group by surname))
union
(select surname, 'min' as Type, amount
from people
where amount in (select min(amount) from people group by surname)
)
order by surname, amount desc;
